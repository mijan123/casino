jQuery(document).ready(function ($) {
	new WOW().init();
	$(".burger-menu").click(function (e) {
		e.preventDefault();

		$(".mobile-menu").toggleClass("active");
	});
	// select2

	$(document).ready(function () {
		$("a.fancybox").fancybox()
	});


	$(window).scroll(function () {

		if ($(document).scrollTop() > 250) {
			$("#detail-header").addClass("detail-header-fixed slideInDown");
		} else {
			$("#detail-header").removeClass("detail-header-fixed slideInDown");
		}
	});
	$.each($(".menu-item-has-children>a"), function (i, d) {
		$(d).append(`
			<button class="dropdown-toggle">
				<i class="fas fa-caret-down"></i>
			</button>
		`)
	});
	$(document).on('click', '.menu-item-has-children a', function (e) {
		event.preventDefault();
		$('.sub-menu').toggleClass('menu-item-active');
		$('.menu-item-has-children').toggleClass('submenu-open');
		$(this).nextAll('ul').eq(0).slideToggle();
		$(this).nextAll('i').eq(0).toggleClass("fa-caret-down fa-caret-up");
	});
	$('.detail-menu ul li ').on('click', function (e) {
		e.preventDefault();
		$(this).siblings().removeClass('active');
		$(this).addClass('active');
		var id = $(this).find('a').attr('href');
		var offset = $(id).offset() && $(id).offset().top;
		if (offset) {
			$('html,body').animate({
				scrollTop: offset - 180
			}, 500);
		}

	});
	$(document).on('click', '.product-float>.close', function (e) {
		event.preventDefault();
		$(this).parent().fadeOut();

	});

	$(window).on("scroll", function () {
		if ($(window).scrollTop() > 100) {

			$(".site-header").addClass("active")
			$(".burger-menu").addClass("active");



		} else {
			$(".site-header").removeClass("active")
			$(".burger-menu").removeClass("active");


		}

	});
	$("#filter").on("click", function (e) {
		$(".primary-wrap").slideToggle();
		$(".mobile-primary-search .close").toggle();
	})

	$(".mobile-primary-search .close").on("click", function (e) {
		$(".mobile-primary-search .filter-wrap").toggle();
	})

	$(document).on('click', '.filter-on', function (e) {
		event.preventDefault();

		$('.filter-sm').eq(0).slideToggle();

	});
	// external js: isotope.pkgd.js

	// init Isotope
	var $grid = $('.grid').isotope({
		itemSelector: '.game-single',
		layoutMode: 'fitRows'
	});
	// filter functions

	// bind filter button click
	$('.filters-button-group').on('click', 'button', function () {
		var filterValue = $(this).attr('data-filter');
		// use filterFn if matches value
		// filterValue = filterFns[filterValue] || filterValue;
		$grid.isotope({ filter: filterValue });
	});
	// change is-checked class on buttons
	$('.button-group').each(function (i, buttonGroup) {
		var $buttonGroup = $(buttonGroup);
		$buttonGroup.on('click', 'button', function () {
			$buttonGroup.find('.is-checked').removeClass('is-checked');
			$(this).addClass('is-checked');
		});
	});

	$('.product-slider').slick({
		autoplay: true,
		infinite: true,
		arrows: true,
		slidesToScroll: 1,
		slidesToShow: 4,

		responsive: [{
			breakpoint: 991,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
			}
		},
		{
			breakpoint: 480,
			settings: {
				dots: false,
				arrows: false,
				slidesToShow: 1,
				slidesToScroll: 1,
			}
		},
		]
	});
	$(function () {
		$('.selectpicker').selectpicker();
	});
	// mobilenav
	// if ($('#nav-menu-container').length) {
	// 	var $mobile_nav = $('#nav-menu-container').clone().prop({
	// 		id: 'mobile-nav'
	// 	});
	// 	$mobile_nav.find('> ul').attr({
	// 		'class': 'mobile-menu',
	// 		'id': ''
	// 	});
	// 	$('body').append($mobile_nav);
	// 	$('body').prepend('<button type="button" id="mobile-nav-toggle"><i class="fa fa-bars"></i></button>');
	// 	$('body').append('<div id="mobile-body-overly"></div>');
	// 	// $('#mobile-nav').find('.menu-item-has-children').prepend('<i class="fa fa-angle-down"></i>');

	// 	$(document).on('click', '.menu-item-has-children i', function (e) {
	// 		$('.sub-menu').toggleClass('menu-item-active');
	// 		$(this).nextAll('ul').eq(0).slideToggle();
	// 		// $(this).toggleClass("fa-angle-up fa-angle-down");
	// 	});

	// 	$(document).on('click', '#mobile-nav-toggle', function (e) {
	// 		$('body').toggleClass('mobile-nav-active');
	// 		$('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
	// 		$('#mobile-nav-toggle').toggleClass('cross');
	// 		$('#mobile-body-overly').toggle();
	// 	});

	// 	$(document).on('click', function (e) {
	// 		var container = $("#mobile-nav, #mobile-nav-toggle");
	// 		if (!container.is(e.target) && container.has(e.target).length === 0) {
	// 			if ($('body').hasClass('mobile-nav-active')) {
	// 				$('body').removeClass('mobile-nav-active');
	// 				$('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
	// 				$('#mobile-body-overly').fadeOut();
	// 			}
	// 		}
	// 	});
	// } else if ($("#mobile-nav, #mobile-nav-toggle").length) {
	// 	$("#mobile-nav, #mobile-nav-toggle").hide();
	// }

	// ----smooth-scrollint---


	// slick-slider
	$('.product-image-slider').slick({
		autoplay: false,
		infinite: true,
		arrows: false,
		slidesToScroll: 1,
		slidesToShow: 1,
		asNavFor: '.product-slider-list',
		responsive: [{
			breakpoint: 991,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
			}
		},
		{
			breakpoint: 480,
			settings: {
				dots: false,
				arrows: false,
				slidesToShow: 1,
				slidesToScroll: 1,
			}
		},
		]
	});
	$('.product-slider-list').slick({
		autoplay: false,
		infinite: true,
		arrows: false,
		slidesToScroll: 1,
		slidesToShow: 4,
		asNavFor: '.product-image-slider',
		focusOnSelect: true,
		ceneterMode: true,
		responsive: [{
			breakpoint: 991,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
			}
		},
		{
			breakpoint: 480,
			settings: {
				dots: false,
				arrows: false,
				slidesToShow: 4,
				slidesToScroll: 1,
			}
		},
		]
	});


});


	// $(window).load(function() {
	//   setTimeout(function() {
	//     $('#preloader').fadeOut(1000);
	//   }, 1000);
	//   setTimeout(function() {
	//     $('#preloader').remove();
	//   }, 1500);




